package lab6.facci.yandrilopez.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
       // Toast.makeText(context,"mensaje recibido",Toast.LENGTH_LONG).show();
        //un control si  es tipo de broadcast y si no es se sale
        if(!intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            return;
        }
        //pdus obtiene los datos del sms
        //a traves de la linea 24
        //accedemos el numero que se envia
        //y el segundo el contenido del sms que hacemos
        Bundle bundle =  intent.getExtras();
        Object[] data = (Object[]) bundle.get("pdus");
        String phone = SmsMessage.createFromPdu((byte[])data[0]).getOriginatingAddress();
        String message =  SmsMessage.createFromPdu((byte[])data[0]).getDisplayMessageBody();
        Toast.makeText(context,parseCode(message),Toast.LENGTH_LONG).show();

    }

    private String parseCode(String message) {
        //pattern y matcher es poder de una cadena de caracteres se puede separar ciertos contenidos de string
        //saca los numeros de un mensaje
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while(m.find()){
            code = m.group(0);
        }
        return  code;

    }
}
